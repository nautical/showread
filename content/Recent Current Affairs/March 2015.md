###Persons

Charles H. Townes, the inventor of the laser, has died, aged 99. He won the 1964 Nobel Prize in physics for research leading to the creation of the laser.

Alberto Nisman, an Argentine prosecutor was found dead in mysterious circumstances just hours before giving what was expected to be damning testimony against President Cristina Kirchner, at his home in the capital Buenos Aires.

King Abdullah, of Saudi Arabia, has died aged 90 after a short illness. His successor is his half-brother Crown Prince Salman.

Krishna Chaudhary, an IPS officer of 1979-batch Bihar cadre, has been appointed as the Director General of the Indo-Tibetan Border Police (ITBP). He succeeds Subhas Goswami. The Indo-Tibetan Border Police was raised in October 1962.

Egypt released the jailed journalist Peter Greste, who will now be deported back to his native Australia. The Egyptian foreign ministry coordinated with the Australian embassy in Cairo for the journalist’s release.


###Places
#####About Boko Haram 

>      Founded in 2002
>       Initially focused on opposing Western education  Boko Haram means ‘Western education is forbidden’ in the Hausa language
>       Launched military operations in 2009 to create Islamic state
>       Thousands killed, mostly in north-eastern Nigeria; also attacked police and UN headquarters in capital, Abuja
>       Some three million people affected
>       Declared terrorist group by the U.S. in 2013

Boko Haram, a radical extremist terror group in Nigeria, is now in control of Baga and 16 neighbouring towns after the military retreated. It is believed that the terrorists killed nearly 2000 people in the town of Baga though other reports put the number in the hundreds. Almost the entire town had been torched and the militants were now raiding nearby areas.

The war between Israel and Gaza drove the Palestinian economy of Gaza and the West Bank into its first contraction since 2006, the International Monetary Fund has said.

###Economy
Prime Minister Narendra Modi’s ambitious initiative financial inclusion scheme Pradhan Mantri Jan Dhan Yojna (PMJDY) has made its way into the Guinness World Records for opening the highest number of bank accounts in the least time.



The Central Government will review after five years the structure of 30,000 life cover given as incentive to the poor for opening bank accounts under the flagship Pradhan Mantri Jan Dhan Yojana (PMJDY).

The direct tax collection during the first nine months of the current fiscal increased by 12.93 per cent to 5.46lakh crore over the same period a year ago. The growth rate of direct tax collections, however, is still short of annual target of 16 per cent.