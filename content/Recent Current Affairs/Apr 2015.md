###Persons
India-born Punit Renjen was appointed the next CEO of U.S.-based accounting giant Deloitte’s global operations, becoming the first Indian-origin person to head a ‘Big-Four’ audit firm.

East Timor independence hero Xanana Gusmao resigned as prime minister, stepping down ahead of an expected restructuring of the government. He is a former guerrilla leader who spearheaded East Timor’s drive for independence when Indonesian rule ended in 2002. He was the first president of the new country, from 2002 to 2007, and then prime minister for more than seven years.

Yemeni President Abedrabbo Mansour Hadi retracted his resignation after escaping house arrest in the militia-controlled capital. The embattled leader had tendered his resignation in January this year after the Shiite militia, known as Houthis, seized the presidential palace and besieged his residence in Sana’a.

People’s Democratic Party (PDP) leader Mufti Mohammed Sayeed was sworn in as the 12th Chief Minister of Jammu and Kashmir. His oath of office and secrecy was administered by Governor N. N. Vohra. The PDP has a coalition with the Bharatiya Janata Party (BJP).

Kolinda Grabar-Kitarovic is the new President of Croatia. 

Boris Nemtsov, the former Deputy Prime Minister of Russia and a leading Russian opposition politician, was shot dead in Moscow.

Renowned American actor Leonard Nimoy has died, aged 83. He was best known for his half-human, half-Vulcan character of Mr Spock in the cult scientific-fiction series ‘Star Trek’. 

Kshatrapati Shivaji, a 1986 batch IAS officer, is the new Chairman and Managing Director (MD) of Small Industries Development Bank of India (SIDBI). Prior to this, he served as the Principal Secretary (Expenditure), Finance Department in Maharashtra government.

###Awards

Indian Businesswomen in Forbes Asia’s 50 Power Businesswomen 2015

>      Arundhati Bhattacharya, CMD, State Bank of India
>      Chanda Kochchar, CEO, ICICI Bank
>      Akhila Srinivasan, MD/Non-Executive Director, Shriram Life Insurance/Shriram Capital
>      Kiran Mazumdar-Shaw, Founder, Chairman and MD, Biocon
>      Shikha Sharma, CEO and MD, Axis Bank
>      Usha Sangwan, MD, Life Insurance Corp. of India


British Academy of Film & Television Arts (BAFTA) Awards 2015

>      Best Film: Boyhood
>      Best Actress: Julian Moore for Still Alice
>      Best Actor: Eddie Redmayne for The Theory of Everything
>      Best Director: Richard Linklater for Boyhood
>      Best Supporting Actor: J. K. Simmons for Whiplash
>      Best Supporting Actress: Patricia Arquette for Boyhood
>      Best Foreign Language: Ida (Poland)


87th Academy Awards (Oscars)

>      Best Film: Birdman
>      Best Actor: Eddie Redmayne for The Theory of Everything
>      Best Actress: Julian Moore for Still Alice
>      Best Director: Alejandro G. Inarritu for Birdman

Bhalchandra Nemade receives Jnanpith Award for 2014

Frei Otto becomes first posthumous winner of Pritzker Prize

Amartya Sen wins J. M. Keynes Prize

###Economy
The Central Government is set to launch a new scheme ‘Krishi Dak Service’ aimed at distribution of improved seed varieties to farmers at their doorstep through post-offices. The government’s agri-research body Indian Agricultural Research Institute (IARI) has already initiated a pilot project in 20 rural districts.

The Foreign Investment Promotion Board (FIPB) has cleared foreign investment in 11 entities, entailing 1,075.91 crore, and referred proposals worth 4,187 crore in pharma firms - Glenmark and Aurobindo - to the Cabinet Committee on Economic Affairs.

Japan’s economy grew 0.6 per cent in the three months to December, as the world’s third largest economy crawled out of recession, the Japanese government said, while the data showed flat growth for the full year.

Wholesale prices fell the most in five-and-a-half years in January this year as decline in oil and some food items resulted in a negative inflation or deflation of 0.39 per cent for the month, raising hopes of rate cut by RBI.

Direct tax collections increased by 11.38 per cent to 5.78 lakh crore during the April-January period of current financial year. The Central Government had collected Rs 5.19 lakh crore in direct taxes during the April-January period of last fiscal, the Finance Ministry said.



The Central Government approved over 8,600 crore of highway projects in Uttar Pradesh, Odisha and Chhattisgarh.


The Central Government approved subsidy on export of up to 1.4 million tonnes of raw sugar in the ongoing 2014-15 marketing year, a move that would boost millers’ cash-flow and enable them to clear mounting sugarcane price arrears that have crossed 12,000 crore.