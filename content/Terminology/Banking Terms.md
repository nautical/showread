Bounced Cheque: when the bank has not enough funds in the relevant account or the account holder requests that the cheque is bounced (under exceptional circumstances) then the bank will return the cheque to the account holder.

BANK RATE: is the rate of interest which is charged by RBI on its advances to commercial banks. When reserve bank desires to restrict expansion of credit it raises the bank rate there by making the credit costlier to commercial bank. 


Current Accounts: These accounts are maintained by the corporate clients that may be operated any number of times in a day. There is a maintenance charge for the current accounts for which the holders enjoy facilities of easy handling, overdraft facility etc.


Cheque Book: A small, bound booklet of cheques. A cheque is a piece of paper produced by your bank with your account number, sort-code and cheque number printed on it. The account number distinguishes your account from other accounts; the sort-code is your bank's special code which distinguishes it from any other bank. 


Cheque Clearing: This is the process of getting the money from the cheque-writer's account into the cheque receiver's account. 


Clearing Bank: This is a bank that can clear funds between banks. For general purposes, this is any institution which we know of as a bank or as a provider of banking services.


Credit Rating: This is the rating which an individual (or company) gets from the credit industry. This is obtained by the individual's credit history, the details of which are available from specialist organisations like CRISIL in India. 


Credit-Worthiness: This is the judgement of an organization which is assessing whether or not to take a particular individual on as a customer. An individual might be considered credit-worthy by one organisation but not by another. Much depends on whether an organization is involved with high risk customers or not. 


Credit Card: A credit card is one of the systems of payments named after the small plastic card issued to users of the system. It is a card entitling its holder to buy goods and services based on the holder's promise to pay for these goods and services.


CRR: Cash reserve Ratio (CRR) is the amount of funds that the banks have to keep with RBI. If RBI decides to increase the percent of this, the available amount with the banks comes down. RBI is using this method (increase of CRR rate), to drain out the excessive money from the banks. 


CASH RESERVE RATIO: specifies the percentage of their total deposits the commercial bank must keep with central bank or RBI. Higher the CRR lower will be the capacity of bank to create credit. 


Demand Deposit: A Demand deposit is the one which can be withdrawn at any time, without any notice or penalty; e.g. money deposited in a checking account or savings account in a bank. 


Debit Card: Debit card allows for direct withdrawal of funds from customers bank accounts. The spending limit is determined by the available balance in the account. 


Fixed Deposits: FDs are the deposits that are repayable on fixed maturity date along with the principal and agreed interest rate for the period. Banks pay higher interest rates on FDs than the savings bank account.


FCNR Accounts: Foreign Currency Non-Resident accounts are the ones that are maintained by the NRIs in foreign currencies like USD, DM, and GBP etc. The account is a term deposit with interest rates linked to the international rates of interest of the respective currencies.


Interest: The amount paid or charged on money over time. If you borrow money interest will be charged on the loan. If you invest money, interest will be paid (where appropriate to the investment). 


Overdraft: This is when a person has a minus figure in their account. It can be authorized (agreed to in advance or retrospect) or unauthorized (where the bank has not agreed to the overdraft either because the account holder represents too great a risk to lend to in this way or because the account holder has not asked for an overdraft facility). 


Internet Banking: Online banking (or Internet banking) allows customers to conduct financial transactions on a secure website operated by the bank. 


Loan: A loan is a type of debt. In a loan, the borrower initially receives or borrows an amount of money, called the principal, from the lender, and is obligated to pay back or repay an equal amount of money to the lender at a later time. There are different kinds of loan such as the house loan, auto loan etc. 


NRE Accounts: Non-Resident External accounts are the ones in which NRIs remit money in any permitted foreign currency and the remittance is converted to Indian rupees for credit to NRE accounts. The accounts can be in the form of current, saving, FDs, recurring deposits. The interest rates and other terms of these accounts are as per the RBI directives. 


OVERDRAFT: It is the loan facility on customer current account at a bank permitting him to overdraw up to a certain agreed limit for a agreed period ,interest is payable only on the amount of loan taken up. 


Payee: The person who receives a payment. This often applies to cheques. If you receive a cheque you are the payee and the person or company who wrote the cheque is the payer. 


Payer: The person who makes a payment. This often applies to cheques. If you write a cheque you are the payer and the recipient of the cheque is the payee.


PRIME LENDING RATE: It is the rate at which commercial banks give loan to its prime customers. 


RBI: The Reserve Bank of India is the apex bank of the country, which was constituted under the RBI Act, 1934 to regulate the other banks, issue of bank notes and maintenance of reserves with a view to securing the monetary stability in India. 


REPO RATE: Under repo transaction the borrower places with the lender certain acceptable securities against funds received and agree to reverse this transaction on a predetermined future date at agreed interest cost. Repo rate is also called (repurchase agreement or repurchase option). 


REVERSE REPO RATE: is the interest rate earned by the bank for lending money tothe RBI in exchange of govt. securities or "lender buys securities with agreement to sell them back at a predetermined rate". 


Recurring Deposits: These are also called cumulative deposits and in recurring deposit accounts, a certain amounts of savings are required to be compulsorily deposited at specific intervals for a specified period. 


Savings Account: Savings account is an account generally maintained by retail customers that deposit money (i.e. their savings) and can withdraw them whenever they need. Funds in these accounts are subjected to low rates of interest. 


Security for Loans: Where large loans are required the lending institution often needs to have a guarantee that the loan will be paid back. This takes the form of a large item of capital outlay (typically a house) which is owned or partly owned and the amount owned is at least equivalent to the loan required. 


SLR: SLR stands for Statutory Liquidity Ratio. This term is used by bankers and indicates the minimum percentage of deposits that the bank has to maintain in form of gold, cash or other approved securities. Thus, we can say that it is ratio of cash and some other approved to liabilities (deposits). It regulates the credit growth in India. 
OR
SLR: known as Statutorily Liquidity Ratio. Each bank is required statutorily maintain a prescribed minimum proportion of its demand and time liabilities in the form of designated liquid asset. 
OR 
"Every bank has to maintain a percentage of its demand and time liabilities by way of cash, gold etc".


Time Deposit: Time deposit is a money deposit at a banking institution that cannot be withdrawn for a certain "term" or period of time. When the term is over it can be withdrawn or it can be held for another term.