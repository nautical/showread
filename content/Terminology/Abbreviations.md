####Financial Full Forms:          
ADB - Asian Development Bank          
ADR - American Depository Receipt          
ASBA – Application Supported by Blocked Amount          
ASSOCHAM - Associated Chambers of Commerce and Industry of India          
ATM - Asynchronous Transfer Mode          
ATM - Automated Teller Machine          
BATM – Biometric Automated Teller Machine          
BoP - Balance of Payment          
BOB – Bank of Baroda          
CAG - Controller and Auditor General of India          
CBDT – Central Board of Direct Taxes          
CBS – Core Banking Services          
CCEA – Cabinet Committee on Economic Affairs          
CD Ratio - Credit Deposit Ratio          
CDR – Corporate Debt Restructuring          
CII - Confederation of Indian Industries          
CP - Commercial Paper          
CPI - Consumer Price Index          
CPI-IW – Consumer Price Index for Industrial Worker          
CRM – Customer Relationship Management      
CRR - Cash Reserve Ratio      
CSO – Central Statistical Organization      
DD - Demand Draft      
DI - Direct Investment      
DSA – Direct Selling Agent      
DTAC – Double Tax Avoidance Convention      
ECB - External Commercial Borrowing      
ECB – European Central Bank      
EMI – Equated (Equal) Monthly Installment      
EPF - Employees Provident Fund      
EPS – Earning Per Share      
ECS - Electronic Clearing Scheme      
ERP – Employee Resource Planning      
EXIM Bank - Export Import Bank of India      
FATF – Financial Action Task Force      
FDI - Foreign Direct Investment      
FEMA - Foreign Exchange Management Act      
FERA – Foreign Exchange Regulation Act (Erstwhile form of FEMA)      
FI - Financial Institution      
FICCI - Federation of Indian Chambers of Commerce and Industry      
FII - Foreign Institutional Investor      
GDP - Gross Domestic Product      
GDR - Global Depository Receipt      
GPF – General Provident Fund      
G-Sec - Government Securities      
GST – Goods and Service Tax      
HDFC - Housing Development Finance Corporation      
HUDCO - Housing & Urban Development Corporation      
IBA – Indian Bank Association      
IBRD - International Bank for Reconstruction and Development      
IBS - International Banking Statistics      
ICAR - Indian Council of Agricultural Research      
ICICI - Industrial Credit and Investment Corporation of India      
IDBI – Industrial Development Bank of India      
IFC - International Finance Corporation      
IFCI - Industrial Finance Corporation of India      
IFRS – International Financial Reporting Standards      
IIBF – Indian Institute of Banking and Finance      
IIM – Indian Institute of Management      
IIP - Index of Industrial Production      
IMD - India Millennium Deposits      
IMF - International Monetary Fund      
INR – Indian Rupee      
IPO – Initial Public Offering      
IPR – Intellectual Property Rights      
IRBI – Industrial Reconstruction Bank of India      
ISO – International Standards Organization      
ISB – Indian School of Business      
ITRS – International Transaction Reporting System      
JLR – Jaguar Land Rover      
KVIC - Khadi & Village Industries Corporation      
KYC – Know Your Customer      
LAF – Liquidity Adjustment Facility      
LBD – Land Development Bank      
LERMS – Liberalized Exchange Rate Management System      
LIBOR – London Inter-Bank Offered Rate      
LIC – Life Insurance Corporation of India      
M1 – Narrow Money      
M3 – Broad Money      
MICR – Magnetic Ink Character Recognition      
MIS – Management Information System      
MIS – Monthly Income Scheme      
MF – Mutual Fund      
MFC – Micro-Finance Companies      
MNC – Multi National Corporation      
MPLADS – Member of Parliament Local Area Development Scheme      
MTM – Mark-To-Market      
NABARD – National Bank for Agriculture and Rural Development      
NAS – National Account Statistics      
NASSCOM – National Association of Software and Services Companies      
NAV – Net Asset Value      
NBC – Non-Banking Companies      
NBFC – Non Banking Financial Companies      
NEFT – National Electronic Fund Transfer      
NFD – Net Fiscal Deficit      
NGO – Non-Governmental Organization      
NHB – National Housing Bank      
NPA – Non-Performing Assets      
NRE Non-Resident External      
NRG – Non-Resident Government      
NRLM – National Rural Livelihood Mission      
NRI – Non-Resident Indian      
NSC – National Statistical Commission      
NSC – National Saving Certificate      
NSSF – National Small Savings Fund      
OD – Over Draft      
OECD – Organisation for Economic Cooperation and Development      
OMO – Open Market Operations      
PDO – Public Debt Office      
P/E – Profit Earnings Ratio      
PF – Provident Fund      
PIO – Persons of Indian Origin      
PMEAC – Prime Minister’s Economic Advisory Committee      
PNB – Punjab National Bank      
PPP – Purchasing Power Parity      
PPP- Private Public Partnership      
PSE – Public Sector Enterprises      
PSU – Public Sector Undertaking      
QIB – Qualified Institutional Buyer      
RBI – Reserve Bank of India      
RD – Recurring Deposit      
RD – Revenue Deficit      
RDBMS – Relational Database Management System      
REC – Rural Electrification Corporation      
RIB – Resurgent India Bonds      
RIDF – Rural Infrastructure Development Fund      
RNBC – Residuary Non-Banking Companies      
RoC – Return on Capital      
RoCs – Registrars of Companies      
RRB – Regional Rural Bank      
RTGS – Real Time Gross Settlement      
SBI – State Bank of India      
SCARDB – State Cooperative Agriculture and Rural Development Bank      
SCB – Scheduled Commercial Bank      
SDR – Special Drawing Right      
SDS – Special Deposit Scheme      
SEBI – Securities and Exchange Board of India      
SEBs – State Electricity Boards      
SFC – State Financial Corporation      
SHGs – Self-Help Groups      
SIDBI – Small Industries Development Bank of India      
SIDC – State Industrial Development Corporation      
SLR – Statutory Liquidity Ratio      
SPF – State Provident Fund      
SSI – Small-Scale Industries      
TBs – Treasury Bills      
UCB – Urban Cooperative Bank      
ULIP – Unit Linked Insurance Plan      
USP – Unique Selling Preposition      
UTI – Unit Trust of India      
VC – Venture Capital      
WPI – Wholesale Price Index      
YTM – Yield to Maturity      
ZCB – Zero Coupon Bond