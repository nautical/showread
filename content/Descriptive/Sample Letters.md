####SAMPLE LETTER 1: 
######FORMAL--You received damaged shoes from a sports company. Write a letter of complaint to the sports company about the issue asking them to reimburse the money you paid.(150 words)

H No. 23/5/A,    
G. K. Colony,    
Market Street.    
Secunderabad-500009.    
September 2, 2012.   
To,    
The Customer Service Manager,    
Customer Service
ABC Sports, LIC Colony,
Mumbai-500001.

Dear Sir,

SUBJECT: Complaint about mistake in honouring an order.

I have recently ordered a new pair of sports shoes from your website.(Order no. 123456; date:July 20, 2012).

I received the package on August 26. Unfortunately, when I opened it, I saw that the shoes were damaged. The shoes had dirt all over them and there was a small tear in the front parts of both the shoes. I have promptly sent the shoes back to you.

To resolve the problem, I request you to credit my account for the amount charged for the shoes; I have already bought a new pair of shoes from another company as I was in urgent need of a pair for my practice.

Thank you for taking time to read this letter. I have been a satisfied customer of your company for many years. This is the first time I have encountered a problem. If you need to contact me, you can reach me at 040-12345678. 

Yours sincerely,

Signature

(name)

####SAMPLE LETTER 2:
######FORMAL--Write an invitation letter inviting an important personality in your community to a presentation ceremony.(150 words)


'Rose Villa',   
Udaynagar Colony,   
Mehdipatnam,   
Hyderabad-500005.   
December 6, 2011.

To
Mr. Raj Kumar,
President
The Lion's Club,
23, R. P. Road,
Secunderabad.

Dear Mr Kumar,

SUBJECT: Invitation to a presentation on corruption.

My name is Ninad Sonawne and I am writing on behalf of the students at Sainik School.

A significant number of the students in our school have been working on a project which relates to the corruption problem in India. We have especially studied the people and administration in your city, Secunderabad. Our project is complete and we are making a presentation on it to the media.

You are invited to attend the presentation that will be held at the Press Club, Hyderabad. We propose to present various aspects and solutions at the presentation.

At the presentation, there will be several students receiving awards which will recognize their contribution. The city Mayor is giving away the awards. Refreshments will also be available at the presentation.

As you are one of the prominent persons in our community, we would be honored by your attendance. Please reply by Monday, the 12th of December to confirm your attendance.

We look forward to seeing you there, 

Yours sincerely,

Signature

(Ninad Sonawne)

####SAMPLE LETTER 3: 
######INFORMAL: Write a letter to a friend describe your summer vacation.

Area 5, H No. 7,    
Srinagar Colony,    
Hyderabad-500012.    
12 December, 2011.    

Dear Swathi,

How are you? I am fine here. It feels like such a long time since the last time I saw you. I know it's only been some months since I saw you last. So far my summer has been great!

I spend all my weekends at the Marina Beach. I am enjoying myself a lotI have been playing lots of games, surfing and building a nice collection of sea shells. Just this past weekend I won a prize in a chess contest.

On the weekdays I work—I have started a summer camp for kids at the beach. It is so cool! The pay isn't too great but I love the job a lot.

I hope the summer's been great for you too. There's only a month and a half left in the summer vacation and after that it's back to college.

We will meet after the vacation is over.

See you; bye.

Yours sincerely,

(Surekha )


####Sample letter 4: 
######INFORMAL Write a letter to your friend who is planning to settle down in a big city. Advise him against the decision.

Address:

Date:

Dear friend,

Received your letter. I am fine here.

In your letter you mentioned that you would like to come to Mumbai for a job. But let me tell you life in Mumbai is not as easy as you think, I would suggest that you take one month's leave from your current job and come and see if it is feasible for you to live here. The biggest problem here is getting a house.

And, the problems will be compounded if you bring your family which you are planning to do. The other big problem is the cost of living which is very high. So think properly before taking a decision and see if you can afford it. This is especially an issue if you have kids. However, it's nice for bachelors to come and make money. I would suggest that you think it over again.

In the end, its your decision and I shall always support your decision.

Hope you decide correctly.

Regards,

Yours sincerely,

(Name)



####Sample letter 5: 
######INFORMAL—Write a letter to your friend for the great time you had in his/her city during the summer.

Address:

Date:

Dear Venu,
I am very fine here. Glad to know that you are fine there. First of all, I would like to say sorry for not writing earlier but I've been studying very hard for my Bank Exam. And by God's grace, I passed it!
I'm writing this letter to thank you for letting me stay with you and your family last summer. It was such a wonderful time that now it's hard for me to go back to my usual life!
Hyderabad is a great city. It is one of the best cities in India. The Charminar, the Salar Jung Museum, etc were magnificent. However, the time I spent with your family members was the greatest.
I'm really looking forward to meeting you again!
That's all for now. I must start my preparation for the interviews.
Please write soon and tell me all your news.   
Give my love to your family.  
Regards,  
Yours sincerely,  
(Leela)
