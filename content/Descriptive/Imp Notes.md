####The Descriptive Writing Test is a vital part of the Bank PO and Clerical exams.

Through the descriptive writing test, banks can assess a candidate’s awareness of his surroundings (general knowledge and general awareness of current affairs/issues etc.), reasoning and analytical skills, ability to organize ideas in a coherent manner and of course, English language skills.    
Now, writing skills require a great deal of practice and a good amount of knowledge. If these are not taken seriously, it may result in a poorly written essay and, consequently, disqualification.    
###(A) Types of questions    
Four types of questions asked.    
#####i. Essay Writing :
A topic is given and one has to write an essay in the space provided within the limited time available.    
#####ii. Paragraph Writing : 
A topic is given and one has to write a paragraph of about 100 words. Generally, proverbs, common sayings or simple questions are given as topics.    
#####iii. Summary Writing : 
A passage is given and the candidates are asked to write the précis/summary of the passage.    
#####iv. Point of view writing : 
The candidates are asked to write the points of view of two or three different persons about an issue, topic, problem etc..    
#####v. Letter Writing : 
One needs to write a formal or informal letter, on given situations or aspects, in the space provided.    
###(B) Important points to be kept in mind   

#####Things to be strictly avoided :    
SCRIBBLING, STRIKING OFF OF SENTENCES, REWRITING, SMUDGING ETC., WHICH INDICATE CLUMSINESS AND CARELESSNESS SHOULD BE STRICTLY AVOIDED.
#####Time Limit :    
The overall time limit is given and depends on the number of writing tasks to be carried out. Generally, 15 minutes per task can be expected.    
#####Word Limit :    
In most cases word limits may not be stipulated. The banks provide space after the topic depending on what type of topic it is. One can write, for instance, an essay of about 150 to 200 words .    
#####Layout and Appearance :    
Every piece of writing should follow the normal standards of style, format etc. Margins, para margins, para divisions, punctuation, spacing, and above all, clear and legible handwriting are paramount.    
#####Marks :    
Generally, 15 to 20 marks are awarded per writing task.    
#####Qualifying Marks :    
The qualifying mark may vary from bank to bank. It is generally 40% of the total marks. However, students should aim for 50% of the total marks to be on the safe side.    
#####Attempts :    
It is advisable that students attempt all the questions as this would ensure that they place themselves in contention for qualification.    