#####Topic 1: Elected representatives of the people should be educated  
#####FOR:
1. It is desirable for the voters to get a highly educated person as their representatives in the Assembly or Parliament. If that is not possible, he must have a stipulated minimum qualification.
2. Only an educated person can understand the intricate legal issues involved in the ruling set up.
3. An educated person can express his ideas in the legislature in a better way than an uneducated person.
4. An educated person can win the respect of the people in his constituency. An uneducated person wins only
sympathy.
5. A representative will have the necessity of discussing vital problems with top executives, industrialists , foreigners,
etc. If heis not educated, he will have to cut a sorry figure.
6. Most of the representatives have a chance of becoming Ministers or members of important committees. If they are
not educated, they are sure to fail in discharging their duties tolerably well.
7. An M.L.A. or M.P. has to do the preparation work by reading a lot. If he is uneducated he will have to depend on
others for everything.

#####AGAINST:
1. In a democratic set­up everyone, including a criminal, has the right of being a voter or a representative of the people.
2. A citizen cannot be prohibited from becoming a representative for want of education unless it is constitutionally stipulated.
3. Lack of education does not deter anyone from becoming a sincere representative. In the past uneducated representatives, atleast a few of them, have won recognition, for their gentlemanly behaviour.
4. A representative needs to be honest and earnest about his duty. And, these can be there in anyone, educated or not.
5. An uneducated representative can understand the masses better than others and most of the masses themselves are uneducated.

#####Topic 2: Banning of bandhs    
#####FOR:
1. Bandhs are organised protests in a small area or the whole state. It will badly affect the non participants.
2. Almost all bandhs are marked by violent attacks on those who do not agree to observe the bandhs.
3. The activists of bandhs throw stones at public vehicles and destroy them. The innocent commuters of buses are
usually attacked and injured.
4. Shop keepers who do not favour bandhs are the worst affected. Their shops are destroyed and they get injured.
5. Even vehicles carrying patients requiring emergency care to the nearby hospitals are affected, sometimes reaching
the hospitals too late.
6. Normal life is paralysed; vehicles are stopped; students are forced to go out of their classes.
7. The common man, especially the daily­wage labourer, is affected most and so a good Govt. should ban bandhs
and protect citizens from the violence committed by the bandh activists.

#####AGAINST:
1. In democracy, the citizens have the rights to join together and to protest against the unjust decisions taken by the government or other authorities.
2. Citizens have the freedom of expression. They can always tell others their opinion on something done by the government. They can protest against injustice in a peaceful manner.
3. The citizens are compelled to observe bandh because the authorities refuse to pay attention to their requests or demands. Bandhs can be avoided if such authorities look into the protesters’ requests and complaints.
4. Political leaders consider bandhs an occasion to attract their followers and assure their leadership.
5. The arrests and police lathicharge following a bandh help create martyrs for the people concerned. Banning
deprives them of the golden opportunity.

#####Topic 3: Should the Government give free mobiles BPL families?    
#####FOR:
1. The government's proposal to provide free mobiles to BPL families is an innovative idea.
2. These days, mobile phones are as necessary as any other electric device in our everyday life.
3. The mobile phone is an indicator of socio­economic empowerment.
4. If given, especially along with free local talk time, it will be a great gift to the poor of India who feel left out in the communication revolution.
5. It will be an indirect boost to the economy as a greater communication always leads to greater business at all levels.
6. 6. The mobiles can also help the government in easy implementation of its schemes.

#####AGAINST:
1. The government's proposal to give free mobile phones to the poor is an impractical idea.
2. The delivering is very difficult; it is almost impossible to ensure that all the poor receive one. 3. It shall be too big a cause for the exchequer to manage.
4. There's every possibility of corruption and yet another scandal.
5. The poor of India are too busy with their livelihood activities to use the cell phone.
6. Most of the Indian poor do not need a cell phone.


#####Topic 4: Cell Phones should be banned in college campuses.    
#####FOR:
1. Cell phones are a great hindrance in the imparting of education which is why students go to colleges.
2. Students often use cell phones to play games, browse the net or chat which waste a lot of precious study time.
3. Cell phones are often abused by some, such as taking pictures of girl students without their knowledge thus offending them socially.
4. Cell phone, especially the high­end and costly smartphones, often flaunted by many, create a divide among the students causing unnecessary strife.
5. Cell phones are also known to have health hazards.
6. In case of emergencies, the parents and students alike may use the college phones or the pay phones available in college canteens.


#####AGAINST:
1. Cell phones make the general communication between the students and the management easier. For instance, sudden 'bunds' which are a common emergency, need to be communicated to students on short notice.
2. Cell phones can be used for effective imparting of education in this world of communication revolution.
3. Cell phones can help in emergency communication between parents and students. These days are greatly volatile days, and anything may happen anytime which may have to be communicated to the parents by students or vice versa. Moreover, colleges are far from homes these days making this use of cell phones critical.
4. There are a lot of educational apps in the smart phones these days that give additional and relevant knowledge to students making them more prepared for the competition in the market.
5. Cell phones make it easy for students, who are almost always on the go, to keep themselves abreast of the latest information in this communication age.
6. Cell phone is a part of the communication revolution and so it would be unfair to deprive students of this device.

#####Topic 5: Women are better managers than women.    
#####FOR:
1. Women have naturally greater patience than men and as patience is important in management, women fare better than men.
2. Women are a natural at multitasking which enables effective management in the fast lane that life is today.
3. Women are better at communication and people skills than men and, as we all know, these are critical in management.
4. Men tend to be more egocentric, impatient, short­tempered than women which works against effective management.
5. Women tend to recognize and appreciate contributions of subordinates and the same is not done by men. People tend to do more when recognized and appreciated.


#####AGAINST:
1. Men being traditionally stronger than women, tend to be more assertive in workplaces than women and this is crucial to management.
2. Men do not have additional burdens of managing home or family affairs which enable them to manage concentrate more on the job at hand.
3. In these modern days, traveling is an important part of work life and men find this working in their favour.
4. Women tend to be jealous of other women leading to ego clashes at workplaces which may hamper their management.